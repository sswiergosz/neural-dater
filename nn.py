import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import numpy as np
import pandas as pd
import string
import sys
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import HashingVectorizer

# parameters
NUM_HIDDEN = 10
NUM_HIDDEN2 = 15
NUM_EPOCHS = 50
LEARNING_RATE = 0.0005
TRAIN_PATH = sys.argv[1] #'train/train-filtered-100.tsv'
TEST_PATH = sys.argv[2] #'test-A/in-filtered-100.tsv'

class Net(torch.nn.Module):
    def __init__(self, n_feature, n_hidden, n_hidden2, n_output):
        super(Net, self).__init__()
        self.fc1 = torch.nn.Linear(n_feature, n_hidden)
        self.fc2 = torch.nn.Linear(n_hidden, n_hidden2)
        self.fc3 = torch.nn.Linear(n_hidden2, n_output)

    def forward(self, x):
        out = F.relu(self.fc1(x))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

def preprocess_text(txt):
    LEN_TXT = len(txt)
    # read NLTK stopwords
    stop_words = stopwords.words('polish')

    for i in range(0, LEN_TXT):
        # lowercasing and tokenizing
        tokens = [w.lower() for w in word_tokenize(txt[i])]

        # removing punctuation and whitespaces
        table = str.maketrans('', '', string.punctuation)
        stripped = [w.translate(table) for w in tokens]
        words = [word for word in stripped if word.isalpha()]

        # removing stopwords and updating value in df
        stop_words = set(stopwords.words('polish'))
        words = [w for w in words if not w in stop_words]
        txt.at[i] = ' '.join(words)
    return txt

def train(model, device, optimizer, txt, expected, loss_func, LEN_TXT):
    model.train()
    total_loss = 0
    for epoch in range(0, NUM_EPOCHS):
        
        for i in range(LEN_TXT):
            model.zero_grad()

            sentence_in = torch.FloatTensor(txt[i]).to(device)
            targets = torch.FloatTensor(np.array(expected[i])).to(device)

            prediction = model(sentence_in)

            loss = loss_func(prediction, targets)
            loss.backward()
            optimizer.step()
            total_loss += loss.item()
        # print('Epoka {}: , Loss: {}, Total loss: {}'.format(epoch, loss, total_loss))

def test(model, txt, device, LEN_TXT):
    model.eval()
    with torch.no_grad():
        for i in range(LEN_TXT):
            print('{}'.format(model(torch.FloatTensor(txt[i])).data[0]))

def main():
    torch.manual_seed(1)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # read and process text data
    train_set = pd.read_csv(TRAIN_PATH, sep='\t', header=None)
    test_set = pd.read_csv(TEST_PATH, sep='\t', header=None)
    X = preprocess_text(train_set[train_set.columns[4]])
    test_data_X = preprocess_text(test_set[test_set.columns[0]])
    y = train_set[train_set.columns[0]]

    LEN_TXT_TRAIN = len(train_set[train_set.columns[4]])
    LEN_TXT_TEST = len(test_set[test_set.columns[0]])

    # hashing trick
    vectorizer = HashingVectorizer()
    transformed_X = vectorizer.fit_transform(X).toarray()
    transformed_test_X = vectorizer.transform(test_data_X).toarray()
    # num_feature = number of all unique words in input
    model = Net(n_feature=transformed_X.shape[1], n_hidden=NUM_HIDDEN, n_hidden2=NUM_HIDDEN2, n_output=1).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)
    loss_func = torch.nn.MSELoss()

    train(model, device, optimizer, transformed_X, y, loss_func, LEN_TXT_TRAIN)   
    test(model, transformed_test_X, device, LEN_TXT_TEST)
    
if __name__ == "__main__":
    main()
    
